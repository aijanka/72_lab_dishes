import axios from 'axios';
import * as actionTypes from './actionTypes';

export const dishesRequest = () => ({type: actionTypes.DISHES_REQUEST});
export const dishesSuccess = dishes => ({type: actionTypes.DISHES_SUCCESS, dishes});
export const dishesFailure = error => ({type: actionTypes.DISHES_FAILURE, error});

export const loadDishes = () => {
    return (dispatch, getState) => {
        dispatch(dishesRequest());
        axios.get('/dishes.json').then(response => {
            dispatch(dishesSuccess(response.data));
        }, error => {
            dispatch(dishesFailure(error));
        })
    }
};


export const addDishSuccess = () => ({type: actionTypes.ADD_DISH_SUCCESS});
export const addDishFailure = error => ({type: actionTypes.ADD_DISH_FAILURE, error});
export const  addDish = (dish) => {
    console.log(dish);
    return (dispatch) => {
        axios.post('/dishes.json', {...dish})
            .then(response => {
            console.log(response);
            dispatch(addDishSuccess());
            loadDishes();
        }, error => {
            dispatch(addDishFailure());
        })
    }
};


export const deleteDishSuccess = () => ({type: actionTypes.DELETE_DISH_SUCCESS});
export const deleteDishFailure = error => ({type: actionTypes.DELETE_DISH_FAILURE, error});
export const deleteDish = key => {
    return (dispatch) => {
        axios.delete(`/dishes/${key}.json`).then(response => {
            dispatch(deleteDishSuccess());
            loadDishes();
        }, error => {
            dispatch(deleteDishFailure());
        })
    }
}

export const editDishSuccess = () => ({type: actionTypes.EDIT_DISH_SUCCESS});
export const editDishFailure = error => ({type: actionTypes.EDIT_DISH_FAILURE, error});
export const editDish = (key, dish) => {
    return (dispatch) => {
        axios.put(`/dishes/${key}.json`, dish).then(response => {
            console.log(response.data, 'edited dish');
            dispatch(editDishSuccess());
            loadDishes();
        }, error => {
            dispatch(editDishFailure());
        })
    }
}