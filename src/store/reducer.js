import * as actionTypes from './actionTypes';

const initialState = {
    dishes: [],
    orders: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.DISHES_REQUEST:
            return {...state, loading: true};
        case actionTypes.DISHES_SUCCESS:
            return {...state, dishes: action.dishes, loading: false};
        case actionTypes.DISHES_FAILURE:
            return {...state, error: action.error, loading: false};

        case actionTypes.ADD_DISH_SUCCESS:
            return {...state};
        case actionTypes.ADD_DISH_FAILURE:
            return {...state, error: action.error};

        case actionTypes.DELETE_DISH_SUCCESS:
            return {...state};
        case actionTypes.DELETE_DISH_FAILURE:
            return {...state, error: action.error};

        case actionTypes.EDIT_DISH_SUCCESS:
            return {...state};
        case actionTypes.EDIT_DISH_FAILURE:
            return {...state, error: action.error};


        default :
            return state;
    }
};

export default reducer;