import React, {Component} from 'react';
import {connect} from "react-redux";

import {addDish, deleteDish, editDish, loadDishes} from "../../store/actions";
import Modal from "../../components/UI/Modal/Modal";

import './Dishes.css';
import Dish from "../../components/Dish/Dish";

class Dishes extends Component {

    state = {
        showAddModal: false,
        isEditing: false,
        currentKey: '',
        dish: {
            title: '',
            price: '',
            image: ''
        }
    }

    componentDidMount() {
        this.props.loadDishes();
    }

    componentDidUpdate (prevProps) {
        console.log(prevProps, 'prevProps');
        
    }

    toggleModal = () => {
        this.setState(prevState => ({showAddModal: !prevState.showAddModal}));
    };

    saveCurrentValue = event => {
        event.preventDefault();
        const dish = [...this.state.dish];
        dish[event.target.name] = event.target.value;
        this.setState({dish});
    };

    saveDish = (event) => {
        event.preventDefault();

        this.props.addDish(this.state.dish);
        this.props.loadDishes();
        this.toggleModal();
    };

    // editDish = key => {
    //     const dish = this.props.dishes[key];
    //     this.setState({dish, showAddModal: true, isEditing: true, currentKey: key});
    //
    // };
    //
    // requestEditingDish = (event, key, dish) => {
    //     event.preventDefault();
    //     this.props.requestEditingDish(this.state.currentKey, this.state.dish);
    //     this.props.loadDishes();
    // }

    render () {
        const keys = this.props.dishes ? Object.keys(this.props.dishes) : [];

        return (
            <div className="DishesWrapper">
                <Modal show={this.state.showAddModal} close={this}>
                    <form className='AddDishModal'>
                        <label>Title</label>
                        <input
                            type="text"
                            name='title'
                            placeholder="Enter dish title"
                            value={this.state.dish.title}
                            onChange={this.saveCurrentValue}
                        />

                        <label>Price</label>
                        <input
                            type="number"
                            name='price'
                            placeholder="Enter dish price"
                            value={this.state.dish.price}
                            onChange={this.saveCurrentValue}
                        />

                        <label>Image</label>
                        <input
                            type="text"
                            name='image'
                            placeholder="Enter dish image url"
                            value={this.state.dish.image}
                            onChange={this.saveCurrentValue}
                        />
                        {}
                        <button onClick={event => this.saveDish(event)}>Save</button>
                        {/*<button onClick={(event) => {this.state.isEditing ? this.requestEditingDish(event) : this.saveDish(event)}}>Save</button>*/}
                    </form>
                </Modal>

                <h2>Dishes</h2>

                <button onClick={this.toggleModal}>Add new dish</button>
                {keys.map(key => (
                    <Dish
                        key={key}
                        title={this.props.dishes[key].title}
                        image={this.props.dishes[key].image}
                        price={this.props.dishes[key].price}
                        delete={(event) => {
                            event.preventDefault();
                            this.props.deleteDish(key);
                            //does not delete without reload
                            this.props.loadDishes();
                        }}
                        // edit={() => {
                        //     this.editDish(key)
                        // }}
                    />
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes
});

const mapDispatchToProps = dispatch => ({
    loadDishes: () => dispatch(loadDishes()),
    addDish: (dish) => dispatch(addDish(dish)),
    deleteDish: (key) => dispatch(deleteDish(key)),
    requestEditingDish: (key, dish) => dispatch(editDish(key, dish))
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);