import React from 'react';
import './Dish.css';
import FontAwesome from 'react-fontawesome';

const Dish = props => (
    <div className="Dish">
        <img src={props.image} alt={props.title}/>
        <div className="DistText">
            <h4>{props.title}</h4>
            <p>{props.price}</p>
            <button className='DishBtns' onClick={props.delete}>
                <FontAwesome
                    className='trash'
                    name='trash'
                    size='2x'
                    style={{textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)'}}
                />
            </button>
            <button className='DishBtns' onClick={props.edit}>
                <FontAwesome
                    className='pencil'
                    name='pencil'
                    size='2x'
                    style={{textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)'}}
                />
            </button>

        </div>
    </div>
);

export default Dish;