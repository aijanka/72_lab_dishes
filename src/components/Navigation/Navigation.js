import React from 'react';
import {NavLink} from "react-router-dom";

import './Navigation.css';

const Navigation = props => (
    <nav className='Navigation'>
        <ul>
            <li><NavLink to='/dishes'>Dishes</NavLink></li>
            <li><NavLink to='/orders'>Orders</NavLink></li>
        </ul>
    </nav>
);

export default Navigation;
