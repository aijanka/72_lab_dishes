import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Navigation from "./components/Navigation/Navigation";
import Dishes from "./containers/Dishes/Dishes";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Navigation/>
                <Switch>
                    <Route path='/dishes' exact component={Dishes}/>
                    {/*<Router path='/orders' component={Orders}/>*/}
                </Switch>
            </Fragment>
        );
    }
}

export default App;
